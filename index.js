// InsertOne Single room in the rooms collection

db.rooms.insertOne({
    
        name: "single",
        accomodates: 2,
        price: 1000,
        description: "A simple room with all the basic necessities",
        rooms_available: 10,
        isAvaible: false
    
})

// InsertMany multiple rooms in the rooms collection

db.rooms.insertMany([


        {
            name: "double",
            accomodates: 3,
            price: 2000,
            description: "A room fit for a small family going on a vacation",
            rooms_available: 5,
            isAvaible: false
        },
        {
            name: "queen",
            accomodates: 4,
            price: 4000,
            description: "A room with a queen sized bed perfect for a simple getaway",
            rooms_available: 15,
            isAvaible: false
        }            
])

//Find()or search a room with a name double

db.rooms.find({name: "double"})


//updateOne method- update the queen room and set the available rooms to 0
db.rooms.updateOne({name: "queen"},{$set:{rooms_available: 0}})

//deleteMany method - delete all rooms that have 0 rooms
db.rooms.deleteMany({rooms_available: 0})

